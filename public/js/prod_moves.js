var movetowish = $('#movetowish').val()
var movetocart = $('#movetocart').val()
$(document).on("click", ".movetowishbtn", function(e) {
    var prod_id = $(this).attr('data-id')
    var t = $(this)
    $.ajax({
        type: 'post',
        url: "/move_to_wish",
        data: { prod_id, "_token": movetowish },
        success: function(r) {
            t.parent().parent().remove()
        }
    });
});
$(document).on("click", ".movetocartbtn", function(e) {
    var prod_id = $(this).attr('data-id')
    var t = $(this)
    $.ajax({
        type: 'post',
        url: "/move_to_cart",
        data: { prod_id, "_token": movetocart },
        success: function(r) {
            if (r) {
                 swal("Oops", "To Be Out Of Stock!", "error")
            } else {
                t.parent().parent().remove()
            }
        }
    });
});
$(document).on("click", ".cartdel", function(e) {
    var prod_id = $(this).attr('data-id')
    var t = $(this)
    $.ajax({
        type: 'post',
        url: "/del_cart",
        data: { prod_id, "_token": movetowish },
        success: function(r) {
            t.parent().parent().remove()
        }
    });
});
$(document).on("click", ".remcart", function(e) {
    var prod_id = $(this).attr('data-id')
    var t = $(this)
    $.ajax({
        type: 'post',
        url: "/rem_cart",
        data: { prod_id, "_token": movetowish },
        success: function(r) {
            if (r == 0) {
                t.parent().parent().parent().remove()
            } else {
                t.parent().parent().find('.cartplus').empty().append(`Count: ${+r['count']}`)
                t.parent().parent().find('.prodtot').empty().append(`<b>Total: ${+r['price']*r['count']}</b>`)

            }
        }
    });
});
$(document).on("click", ".wishdel", function(e) {
    var prod_id = $(this).attr('data-id')
    var t = $(this)
    $.ajax({
        type: 'post',
        url: "/del_wish",
        data: { prod_id, "_token": movetocart },
        success: function(r) {
            t.parent().parent().remove()
        }
    });
});
