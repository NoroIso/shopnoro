var passrec = $('#passrec').val()
var newpsschngtoken = $('#newpsschngtoken').val()
$('.passrecbtn').click(function(event) {
    let passrecmail = $('#email').val()
    $.ajax({
        type: 'post',
        url: "/passrec_ing",
        data: { passrecmail, "_token": passrec },
        success: function(r) {
            if (r) {
                r = JSON.parse(r);
                $('.error').remove();
                if (r['passrecmail']) {
                    $('#email').after(`<div class="error" style='color:red'>${r['passrecmail'][0]}</div>`);
                } else if (r['emailmpty']) {
                    $('#email').after(`<div class="error" style='color:red'>${r['emailmpty'][0]}</div>`);
                }
            } else {
                $('.passrecbody').empty();
                $('.passrecbody').append(`
                	<h1 class="h3 mb-3 font-weight-normal">Reset Password</h1>
                	<label for="email_code" class="sr-only">Code:</label><br>
					<input type="text" name="email_code" id="email_code" value="" class="form-control passrecmail" placeholder="Code" required="" autofocus="">
					<label for="newpss" class="sr-only">New Password</label><br>
					<input type="text" name="newpss" id="newpss" value="" class="form-control passrecmail" placeholder="New Password" required="" autofocus="">
					<label for="newpssconf" class="sr-only">Confirm Password</label><br>
					<input type="text" name="newpssconf" id="newpssconf" value="" class="form-control passrecmail" placeholder="Confirm Password" required="" autofocus="">
					<button class="btn btn-lg btn-primary btn-block my-5 newpsschng">Change Password</button>
					<input type="hidden" id="passrecmail" value="${passrecmail}">
                	`);
            }
        }
    })
});
$(document).on("click", ".newpsschng" , function() {           
let email_code = $('#email_code').val()
let newpss = $('#newpss').val()
let newpssconf = $('#newpssconf').val()
let passrecmail = $('#passrecmail').val()
    $.ajax({
        type: 'post',
        url: "/resetpass",
        data: { newpssconf,newpss,email_code,passrecmail, "_token": newpsschngtoken },
        success: function(r) {
             if (r) {
                r = JSON.parse(r);
                $('.error').remove();
                if (r['email_code']) {
                    $('#email_code').after(`<div class="error" style='color:red'>${r['email_code'][0]}</div>`);
                } 
                if (r['newpss']) {
                    $('#newpss').after(`<div class="error" style='color:red'>${r['newpss'][0]}</div>`);
                }
                if (r['newpssconf']) {
                    $('#newpssconf').after(`<div class="error" style='color:red'>${r['newpssconf'][0]}</div>`);
                }
            } else {
            	$(location).attr('href','/login');
            }
        }
    })
});
