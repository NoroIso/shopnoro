var token1 = $('#token1').val()
var token3 = $('#token3').val()
var id = $('#pr_id').val()
$('.prodedt').click(function(event) {
    let name = $('#name').val()
    let price = $('#price').val()
    let count = $('#count').val()
    let description = $('#description').val()
    $.ajax({
        type: 'post',
        url: "/update_item/" + id,
        data: { name, price, count, description, "_token": token1 },
        success: function(r) {
            if (r) {
                r = JSON.parse(r);
                $('.error').remove();
                if (r['name']) {
                    $('#name').after(`<div class="error" style='color:red'>${r["name"][0]}</div>`);
                }
                if (r['price']) {
                    $('#price').after(`<div class="error" style='color:red'>${r['price'][0]}</div>`);
                }
                if (r['count']) {
                    $('#count').after(`<div class="error" style='color:red'>${r['count'][0]}</div>`);
                }
                if (r['description']) {
                    $('#description').after(`<div class="error" style='color:red'>${r['description'][0]}</div>`);
                }
            } else {
                swal("Good job!", "Success!", "success");
            }
        }
    })
});

$('#openprodphoto').click(function(event) {
    $("#divprodphoto").toggle();
})
$(document).on("click", ".prodphotodelete", function(e) {
    var prodpho = $(this).attr('data-id')
    $.ajax({
        type: 'post',
        url: "/pho_del_product",
        data: { prodpho, "_token": token3 },
        success: function(r) {
            location.reload()
        }
    })
});
