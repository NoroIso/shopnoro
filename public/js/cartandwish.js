var cartwishtoken = $('#cartwishtoken').val()
$(document).on("click", ".add_cartcls", function(e) {
    var prod_id = $(this).attr('data-id')
    var t = $(this)
    $.ajax({
        type: 'post',
        url: "/add_cart",
        data: { prod_id, "_token": cartwishtoken },
        success: function(r) {
            if (r) {
                t.parent().parent().find('.cartplus').empty().append(`Count: ${+r['count']}`)
                t.parent().parent().find('.prodtot').empty().append(`<b>Total: ${+r['price']*r['count']} $</b>`)
                swal("Good job!", "You clicked the button!", "success");
            } else {
                swal("Oops", "To Be Out Of Stock!", "error")
            }
        }
    });
});
$(document).on("click", ".add_wishcls", function(e) {
    var prod_id = $(this).attr('data-id')
    $.ajax({
        type: 'post',
        url: "/add_wishlist",
        data: { prod_id, "_token": cartwishtoken },
        success: function(r) {
            if (r) {
                console.log(r)
            } else {
                swal("Good job!", "You clicked the button!", "success");
            }
        }
    });
});
