var token2 = $('#token2').val()
$("#prod_min_price").on("input", function() {
    var prod_min_price = $("#prod_min_price").val()
    $("#prod_min_price_show").html($("#prod_min_price").val())
})
$("#prod_max_price").on("input", function() {
    var prod_max_price = $("#prod_max_price").val()
    $("#prod_max_price_show").html($("#prod_max_price").val())
})
$("#prod_name").on("input", function() {
    $("#prod_srch").prop( "disabled", false );
})
$("#prod_srch").click(function() {
    var prod_name = $("#prod_name").val()
    var prod_min_price = $("#prod_min_price").val()
    var prod_max_price = $("#prod_max_price").val()
    $.ajax({
        url: "/prod_srch",
        type: "post",
        data: { prod_name, prod_min_price, prod_max_price, "_token": token2 },
        success: function(r) {
            $('.allsrch').empty()
            r.forEach( function(item) {
                var pic=''
                console.log(item)
                if (item.photos[0]) {
                    pic=item.photos[0].url
                }else{
                    pic='specialoffer.png'
                }
            $('.allsrch').append(` 
                <div class="col-md-auto mx-auto my-3">
                <div class="card " style="width:200px">
                <img class="card-img-top" src="../images/${pic}" alt="Card image" style="width:100%;height: 198px">
                <div class="card-body">
                <h4 class="card-title">${item.name}</h4>
                <p class="card-text">${item.price}</p>
                <div class="btn-group">
                <div   data-id="${item.id}" class="btn btn-outline-secondary add_cartcls" >Add Cart</div>
                <div class="btn btn-outline-secondary add_wishcls" data-id="${item.id}"  >Add Wishlist</div>
                </div>
                </div>
                <a href="all_product/item/${item.id}" class="btn btn-light">Details</a>
                </div>
                </div>
                `)
            })
        }
    });
});
$(".srchopbut").click(function() {
    $(".srchbody").toggle()
});
