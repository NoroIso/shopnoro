var token = $('#token').val()
$('.passchange').click(function(event) {
    let oldpassword = $('#oldpassword').val()
    let password = $('#password').val()
    let config_password = $('#config_password').val()
    $.ajax({
        type: 'post',
        url: "/changepass",
        data: { oldpassword, password, config_password, "_token": token },
        success: function(r) {
            if (r) {
                r = JSON.parse(r);
                $('.error').remove();
                if (r['oldpassword']) {
                    $('#oldpassword').after(`<div class="error" style='color:red'>${r['oldpassword'][0]}</div>`);
                }
                if (r['password']) {
                    $('#password').after(`<div class="error" style='color:red'>${r['password'][0]}</div>`);
                }
                if (r['config_password']) {
                    $('#config_password').after(`<div class="error" style='color:red'>${r['config_password'][0]}</div>`);
                }
            } else {
                swal("Good job!", "Success! Your Password has been changed!", "success");
            }

        }
    })
});
