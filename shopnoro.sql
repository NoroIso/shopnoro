/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : shopnoro

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 07/03/2020 16:51:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `surname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Norayr', 'Isahakyan', 50, 'a@anyan.com', 'm', '$2y$10$1zZ1vMmaMHDXuC9at/EBNewU9jVi1.1clJIz1jh48BtvyMgWGSSiG');
INSERT INTO `users` VALUES (2, 'Norayr', 'Isahakyan', 20, 'nisahakyan@list.ru', 'm', '$2y$10$wneIeu7JGyNvUW9BdLTLbOQckEGzmSl9q6ilL5j9GRsQ28aWXlqVu');
INSERT INTO `users` VALUES (3, 'Lilit', 'Isahakyan', 20, 'ketikyan.lilit9@gmail.com', 'f', '$2y$10$Axh2wbJSaYJYrJWyuA/EmenslGgXT7MKEBTyy/p.jQZfhWHiUgxOa');

SET FOREIGN_KEY_CHECKS = 1;
