/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100411
Source Host           : localhost:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 100411
File Encoding         : 65001

Date: 2020-04-21 13:09:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pruduct_id` (`product_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('37', '8', '2', '5');
INSERT INTO `cart` VALUES ('46', '6', '2', '4');
INSERT INTO `cart` VALUES ('58', '8', '5', '2');
INSERT INTO `cart` VALUES ('59', '6', '6', '1');
INSERT INTO `cart` VALUES ('60', '8', '6', '1');
INSERT INTO `cart` VALUES ('61', '2', '6', '1');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `admin_id` (`admin_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `message_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('1', null, '2', 'pp', '2020-04-19 20:41:43');
INSERT INTO `message` VALUES ('2', null, '2', null, '2020-04-19 23:32:46');
INSERT INTO `message` VALUES ('3', null, '2', null, '2020-04-19 23:33:52');
INSERT INTO `message` VALUES ('4', null, '4', null, '2020-04-19 23:35:14');
INSERT INTO `message` VALUES ('5', null, '2', 'fhgf', '2020-04-19 23:35:42');
INSERT INTO `message` VALUES ('6', null, '2', null, '2020-04-19 23:37:50');
INSERT INTO `message` VALUES ('7', null, '2', null, '2020-04-19 23:38:03');
INSERT INTO `message` VALUES ('8', null, '2', null, '2020-04-19 23:38:28');
INSERT INTO `message` VALUES ('9', null, '2', null, '2020-04-20 15:08:39');
INSERT INTO `message` VALUES ('10', null, '2', 'ytut', '2020-04-20 15:10:29');
INSERT INTO `message` VALUES ('12', null, '2', null, '2020-04-20 15:33:58');
INSERT INTO `message` VALUES ('13', null, '2', 'sdfsdfs', '2020-04-20 15:34:07');
INSERT INTO `message` VALUES ('14', null, '2', 'xzcxcz', '2020-04-20 16:27:07');
INSERT INTO `message` VALUES ('19', null, '2', null, '2020-04-20 18:52:51');
INSERT INTO `message` VALUES ('21', null, '2', null, '2020-04-20 18:53:38');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of migrations
-- ----------------------------

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sum` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('1', '2020-04-09 20:38:31', '250', '5');
INSERT INTO `order` VALUES ('2', '2020-04-09 21:08:00', '250', '5');
INSERT INTO `order` VALUES ('3', '2020-04-12 16:14:48', '290', '5');
INSERT INTO `order` VALUES ('4', '2020-04-12 16:15:40', '190', '5');

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `feedback` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_id` (`order_id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE,
  CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of order_details
-- ----------------------------
INSERT INTO `order_details` VALUES ('1', '2', '2', '50.00', '1', 'cxvx vxcxv xcvxcxvxcv xcxvxcvxcxvxcvxcxvxcvxcxvxcvx cxvxcvx cxvx vxcxv xcvxcxvxcv xcxvxcvxcxvxcvxcxvxcvxcxvxcvx cxvxcvx cxvx vxcxv xcvxcxvxcv xcxvxcvxcxvxcvxcxvxcvxcxvxcvx cxvxcvx cxvx vxcxv xcvxcxvxcv xcxvxcvxcxvxcvxcxvxcvxcxvxcvx cxvxcvx cxvx vxcxv xcvx');
INSERT INTO `order_details` VALUES ('2', '2', '8', '100.00', '2', null);
INSERT INTO `order_details` VALUES ('3', '3', '8', '100.00', '1', 'sd');
INSERT INTO `order_details` VALUES ('4', '3', '6', '100.00', '1', null);
INSERT INTO `order_details` VALUES ('6', '4', '8', '100.00', '1', null);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('2', '2', 'apranq 2', '9', '50.00', 'erkrord apranq porcnakan', '0');
INSERT INTO `product` VALUES ('6', '2', 'apranq 5', '9', '100.00', 'fghfghfhfghfhfghfhfhfhfhfhf', '1');
INSERT INTO `product` VALUES ('8', '2', 'apranq 10', '6', '100.00', 'asdasdasadasddasdsadadad', '1');

-- ----------------------------
-- Table structure for product_photo
-- ----------------------------
DROP TABLE IF EXISTS `product_photo`;
CREATE TABLE `product_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE,
  CONSTRAINT `product_photo_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_photo
-- ----------------------------
INSERT INTO `product_photo` VALUES ('4', '8', '15854815521ba48fa53de96f11bc6edcd2d0fe543a.jpg');
INSERT INTO `product_photo` VALUES ('5', '8', '158548155281-TzbzI6XL._SR500,500_.jpg');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT 0,
  `forgot_token` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT 'user',
  `block` int(1) DEFAULT 0,
  `block_time` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('2', 'Ani', 'Sargsyan', '25', 'male', 'a@mail.ru', '$2y$10$if3mCNwTDx1mnfbjwAafnOo9MasphdX3afEK6FFtMPjhV436jr6tu', '1', 'IQHpsGT7fyENeQHEnqys11Cidvpsaafl', 'user', '0', null);
INSERT INTO `users` VALUES ('4', 'Anna', 'Davtyan', '20', 'female', 'an@mail.ru', '$10$OvsNR8NuwtenfXDudGdNReFMiLFud4Z47Hb1vcAyIq3WNuRuUhCsO', '0', null, 'user', '0', null);
INSERT INTO `users` VALUES ('5', 'Eduard', 'Tovmasyan', '25', 'male', 'edotovmasyan21@gmail.com', '$2y$10$B/SAWuo/Q8IINnYIGhtGRe2z4cp4mILICGJkrpgZbmyTSGQiim0mi', '1', 'UlCuHgpml1EuVG0VjXKrQ3UJxbtbp4Wu', 'user', '0', '2020-04-20 22:14:48');
INSERT INTO `users` VALUES ('6', 'Norayr', 'Isahakyan', '21', 'male', 'norayrisahakyan@gmail.com', '$2y$10$y4uq4DpZaIpwQHmyotbtnu0Bbi1eDVk8tn94lrlLhQgEthGQ53upe', '1', 'ihlqMjDmP1QYEMEH7GPAAJzA00CAsLkmez', 'admin', '0', '2020-04-20 22:19:28');

-- ----------------------------
-- Table structure for wishlist
-- ----------------------------
DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pruduct_id` (`product_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of wishlist
-- ----------------------------
INSERT INTO `wishlist` VALUES ('26', '8', '2');
SET FOREIGN_KEY_CHECKS=1;
