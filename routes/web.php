<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::get('/das1',"das1@das1_show");
Route::get('/das1/home',"das1@das1_print");
Route::get('/signup',"userController@show_signup");
Route::post('/register',"userController@register");
Route::get('/login',"userController@show_login");
Route::post('/login',"userController@login");
Route::get('/logout',"userController@log_out");
Route::get('/verify/{hash}/{id}',"userController@verify_account");
Route::get('/password/recover',"userController@show_password_recover");
Route::post('/passrec_ing',"userController@password_recover");
Route::post('/resetpass',"userController@changeforgotenpass");

Route::group(['middleware'=>['checkLogin']],function(){
Route::get('/profile',"userController@show_profile");
Route::get('/edit',"userController@show_edit");
Route::post('/change',"userController@edit");
Route::get('/add_product',"ProductController@show_add_product");
Route::post('/add_product',"ProductController@add_product");
Route::get('/myproduct',"userController@show_myproduct");
Route::get('/allproduct',"userController@show_allproduct");
Route::post('/changepass',"userController@changepass");
Route::get("/product/item/{id}", "ProductController@item"); 
Route::get("/all_product/item/{id}", "ProductController@all_item"); 
Route::get('/delete_item/{id}',"ProductController@delete_item");
Route::post('/update_item/{id}',"ProductController@update_item");
Route::post('/pho_del_product',"ProductController@pho_del_product");
Route::post('/prod_srch',"ProductController@prod_srch");
Route::post('/add_cart',"ProductController@addTocart");
Route::post('/add_wishlist',"ProductController@addTowishlist");
Route::get('/mywishlist',"userController@show_wishpage");
Route::get('/mycart',"userController@show_cartpage");
Route::post('/move_to_cart',"ProductController@move_to_cart");
Route::post('/move_to_wish',"ProductController@move_to_wish");
Route::post('/del_cart',"ProductController@del_cart");
Route::post('/del_wish',"ProductController@del_wish");
Route::post('/rem_cart',"ProductController@rem_cart");
Route::get('/cart/checkout',"StripeController@stripe_show");
Route::post('stripe', 'StripeController@stripePost')->name('stripe.post');
Route::get('/myorder',"userController@show_myorder");
Route::get("/myorder/item/{id}", "ProductController@order_item");
Route::post('/feedback',"ProductController@feedback");
Route::get("/admin", "AdminController@admin_view");
Route::get('/admin{any}',function(){
	return view('admin');
})->where('any','.*');



});






