@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="container my-5">
  <div class="row">
    <div class="col-sm-4">
      @if(isset($productinfo->Photos[0]->url))
      <img class="card-img-top" src="{{ asset('/images/'.$productinfo->Photos[0]->url) }}" alt="Card image" style="width:100%">
      @else
      <img class="card-img-top" src="{{ asset('/images/specialoffer.png') }}" alt="Card image" style="width:100%">
      @endif
      <button id="openprodphoto" type="button" class="btn btn-secondary btn-block fa fa-arrow-down" aria-hidden="true">All Photos</button>
    </div>
    <div class="col-sm-8">
      <h3>Name : {{$productinfo['name']}}</h3>
      <h3>Price : {{$productinfo['price']}}$</h3>
      <h3>Stock : {{$productinfo['count']}}</h3>
      <h6>Description : {{$productinfo['description']}}</h3>
      <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
        <div class="btn-group">
          <div  data-id="{{$productinfo['id']}}" class="btn btn-outline-secondary add_cartcls" >Add Cart</div>
          <div class="btn btn-outline-secondary add_wishcls" data-id="{{$productinfo['id']}}">Add Wishlist</div>
        </div>
      </div>
    </div>
  </div>
  <div id="divprodphoto" class="row my-5" style="display: none;">
    <div class="col-md-auto mx-auto my-3">
      @if(isset($productinfo->Photos[0]->url))
      @foreach ($productinfo->Photos as $p)
      <div class="mx-3 my-5" style="display:inline-block;position:relative;">
        <img  src="{{ asset('/images/'.$p['url']) }}" alt="Card image" width="200px" height="200px">
      </div>
      @endforeach
      @else
      <img class="" src="{{ asset('/images/specialoffer.png') }}" alt="Card image" width="200px" height="200px">
      @endif
    </div>
  </div>
</div>
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection