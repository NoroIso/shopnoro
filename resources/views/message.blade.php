@extends('layouts.app2')
<!-- body content -->
@section('content')
@endsection
<div id="messageBox">
  <div id="messangerTop">
    
    <div class="leftArrow"></div>
    <div id="portrait"></div>
    <div id="userInfo">
    @foreach($message as $m)
      <div id="userName">$m->message->id</div>
      <div id="userStatus">online</div>
    </div>
  </div>
  
  <div id="messangerContent">
    <div class="time">$m->message->time</div>
    <div class="message left">$m->message->message</div>
  </div>
  
  <div id="messangerBottom">
    <div id="smile">
      <div id="eye1"></div>
      <div id="eye2"></div>
      <div id="mouth"></div>
    </div>
    <input id="messageInput"/>
    <div class="icon icon-mic">
        <div class="icon-mic-dot"></div>
        <div class="icon-mic-circle"></div>
        <div class="icon-mic-line"></div>
    </div>
  </div>
  @endforeach
</div>
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/mess.css') }}">
@endsection