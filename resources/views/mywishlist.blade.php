@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="container my-5">
  <div class="row">
    @foreach($product as $p)
    <div class="col-md-auto mx-auto my-3">
      <div class="card " style="width:200px">
        @if(isset($p->Wish->Photos[0]->url))
        <img class="card-img-top" src="{{ asset('/images/'.$p->Wish->Photos[0]->url) }}" alt="Card image" style="width:100%;height: 198px">
        @else
        <img class="card-img-top" src="{{ asset('/images/specialoffer.png') }}" alt="Card image" style="width:100%">
        @endif
         <img data-id="{{$p->Wish->id}}" class="position-absolute wishdel" src="{{ asset('/images/itemdel.png') }}" width="25px" height="25px" style="left: 93%;bottom: 97%">
        <div class="card-body">
          <h4 class="card-title">{{$p->Wish->name}}</h4>
          <p class="card-text">Price: {{$p->Wish->price}}$</p>
        </div>
        <div class="btn btn-light movetocartbtn" data-id="{{$p->Wish->id}}"> Move To Cart</div>
        <a href="product/item/{{$p->Wish->id}}" class="btn btn-light">Details</a>
      </div>
    </div>
    @endforeach
  </div>
</div>
<div class="row">
  <div class="mx-auto">{{$product->links()}}</div>
</div>
<input type="hidden" id="movetocart" value="{{csrf_token()}}">
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection