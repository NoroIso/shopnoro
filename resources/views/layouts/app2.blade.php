<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shop</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rating/1.5.0/bootstrap-rating.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/footer.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/proddel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/prod_button.css') }}">
    @yield('css')
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="{{url('profile')}}">SHOP</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navb">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{url('myproduct')}}">My Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('allproduct')}}">All Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('mycart')}}">My Cart</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('mywishlist')}}">My WishList</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('myorder')}}">My Orders</a>
          </li>
        </ul>
        <ul class="navbar-nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle mr-5" href="#" id="navbardrop" data-toggle="dropdown">
              Personal
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{url('profile')}}">@yield('name_surname')</a>
              <a class="dropdown-item" href="{{url('edit')}}">Edit</a>
              <a class="dropdown-item" href="{{url('add_product')}}">Add Product</a>
              <a class="dropdown-item" href="{{url('logout')}}">Log Out</a>
            </div>
          </li>
        </ul>
        <!--  <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" placeholder="Search">
          <button class="btn btn-success my-2 my-sm-0" type="button">Search</button>
        </form> -->
      </div>
    </nav>
    <!--content  -->
    @yield('content')
    <input type="hidden" id="cartwishtoken" value="{{csrf_token()}}">
    <!--endcontent  -->
    <!-- Site footer -->
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>About</h6>
            <p class="text-justify">LOREM <i>Lorem ipsum dolor sit amet </i> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum..</p>
          </div>
          <div class="col-xs-6 col-md-3">
            <h6>Categories</h6>
            <ul class="footer-links">
              <li><a href="{{url('allproduct')}}">All Products </a></li>
              <li><a href="{{url('myproduct')}}">My Products </a></li>
              <li><a href="{{url('mycart')}}">My Cart</a></li>
              <li><a href="{{url('mywishlist')}}">My WishList</a></li>
              <li><a href="{{url('myorder')}}">My Orders</a></li>
              <!-- <li><a href="#">Templates</a></li> -->
            </ul>
          </div>
          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
              <li><a href="#">About Us</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Contribute</a></li>
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Sitemap</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2020 All Rights Reserved by
              <a href="#">Eduard Tovmasyan</a>.
            </p>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  </body>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript" src="{{asset('js/passchange.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/productedit.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/proditem.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/cartandwish.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/prod_moves.js')}}"></script>
    @yield('js')
</html>