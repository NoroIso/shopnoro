@extends('layouts.app2')
<!-- body content -->
@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="{{ asset('/images/promo1.jpg') }}" alt="Shop"  style="width:100%;max-height: 700px">
      <div class="carousel-caption">
        <h3>Rachel Zoe</h3>
        <p>“Style is a way to say who you are without having to speak.”</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="{{ asset('/images/promo2.jpg') }}" alt="Shop" style="width:100%;max-height: 700px" >
      <div class="carousel-caption">
        <h3>Coco Chanel</h3>
        <p>“In order to be irreplaceable one must always be different.”</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="{{ asset('/images/promo3.jpeg') }}" alt="Shop"  style="width:100%;max-height: 700px">
      <div class="carousel-caption">
        <h3>Jess C. Scott</h3>
        <p>“The human body is the best work of art.”</p>
      </div>
    </div>
  </div>
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
<div class="my-5" style="width:100%;text-align: center;"></div>
<div class="container my-5">
  <div class="row">
    @foreach($product as $p)
    <div class="col-md-auto mx-auto my-3">
      <div class="card " style="width:200px">
        <!-- <h1>{{$p->Seller->name}}</h1> -->
        @if(isset($p->Photos[0]->url))
        <img class="card-img-top proditem" src="{{ asset('/images/'.$p->Photos[0]->url) }}" alt="Card image" style="width:100%;height: 198px">
        @else
        <img class="card-img-top proditem" src="{{ asset('/images/specialoffer.png') }}" alt="Card image" style="width:100%">
        @endif
        <div class="card-body">
          <h4 class="card-title">{{$p['name']}}</h4>
          <p class="card-text">Price: {{$p['price']}}$</p>
          <div class="btn-group">
            <div  class="btn btn-outline-secondary add_cartcls" data-id="{{$p['id']}}">Add Cart</div>
            <div  class="btn btn-outline-secondary add_wishcls" data-id="{{$p['id']}}">Add Wishlist</div>
          </div>
        </div>
        <a href="all_product/item/{{$p['id']}}" class="btn btn-light">Details</a>
      </div>
    </div>
    @endforeach
    
  </div>
</div>
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection