@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="container my-5">
	<!-- <div class="col-md-8 order-md-1"> -->
	<form  method="post" action="{{url('change')}}" class="needs-validation">
		<div class="mb-3">
			<label for="name">Name:</label><br>
			<input type="text" name="name" id="name" value="{{$data['name']}}" class="form-control">
			{{$errors->first("name")}}
		</div>
		<div class="mb-3">
			<label for="surname">Surname:</label><br>
			<input type="text" name="surname" id="surname" value="{{$data['surname']}}" class="form-control">
			{{$errors->first("surname")}}
		</div>
		<div class="mb-3">
			<label for="age">Age:</label><br>
			<input type="" name="age" id="age" value="{{$data['age']}}" class="form-control">
			{{$errors->first("age")}}
		</div>
		<label for="email">Email:</label><br>
		<input type="text" name="email" id="email" value="{{$data['email']}}"  class="form-control">
		{{$errors->first("email")}}
		<!-- Button to Open the Modal -->
		<button type="button" class="btn btn-link my-3" data-toggle="modal" data-target="#myModal">
		Change Password
		</button>
		<button class="btn btn-primary btn-lg btn-block">Save Changes</button>
		{{csrf_field()}}
	</form>
	<!-- The Modal -->
	<div class="modal" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Modal Heading</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<!-- Modal body -->
				<div class="modal-body">
					<div class="mb-3">
						<label for="oldpassword">Old Password:</label><br>
						<input type="text" name="oldpassword" id="oldpassword" value="{{old('oldpassword')}}" class="form-control">
					</div>
					<div class="mb-3">
						<label for="password">Password:</label><br>
						<input type="text" name="password" id="password" value="{{old('password')}}" class="form-control">
					</div>
					<div class="mb-3">
						<label for="config_password">Config Password:</label><br>
						<input type="text" name="config_password" id="config_password" value="{{old('config_password')}}" class="form-control">
					</div>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					<button class="btn btn-primary passchange">Change Password</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
				<input type="hidden" id="token" value="{{csrf_token()}}">
			</div>
		</div>
	</div>
</div>
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection