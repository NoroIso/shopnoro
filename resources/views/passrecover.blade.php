@extends('layouts.app')
@section('content')
<div class="container">
	<div class="text-center mx-auto my-5 passrecbody" style="width: 50%;height: 100vh" >
			<h1 class="h3 mb-3 font-weight-normal">Forgot Password</h1>
			<label for="email" class="sr-only">Email Address</label><br>
			<input type="text" name="email" id="email" value="{{old('email')}}" class="form-control passrecmail" placeholder="Email address" required="" autofocus="">
			<button class="btn btn-lg btn-primary btn-block my-5 passrecbtn">Send Mail</button>
			<input type="hidden" id="passrec" value="{{csrf_token()}}">
	</div>
	<input type="hidden" id="newpsschngtoken" value="{{csrf_token()}}">
</div>
@endsection