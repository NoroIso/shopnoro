@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="col-md-8 order-md-1 my-5">
	<form  method="post" action="{{url('add_product')}}" class="needs-validation" enctype="multipart/form-data">
		<div class="mb-3">
			<label for="name">Product Name:</label><br>
			<input type="text" name="name" id="name" value="{{old('name')}}" class="form-control">
			{{$errors->first("name")}}
		</div>
		<div class="mb-3">
			<label for="count">Count:</label><br>
			<input type="" name="count" id="count" value="{{old('count')}}" class="form-control">
			{{$errors->first("count")}}
		</div>
		<div class="mb-3">
			<label for="price">Price:</label><br>
			<input type="" name="price" id="price" value="{{old('price')}}" class="form-control">
			{{$errors->first("price")}}
		</div>
		<div class="mb-3">
			<label for="price">Photo:</label><br>
			<input type="file" name="photo[]" multiple class="form-control">
			
		</div>
		<div class="mb-3">
			<label for="description">Description:</label><br>
			<textarea name="description" id="description" value="{{old('description')}}" class="form-control"></textarea>
			{{$errors->first("description")}}
		</div>
		<button class="btn btn-primary btn-lg btn-block">Add Product</button>
		{{csrf_field()}}
	</form>
</div>
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection