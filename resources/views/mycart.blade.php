@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="container my-5">
  <div class="row my-5">
    <div class="col-md-10 "></div>
    <div class="col-md-2 "><a  class="btn btn-primary" href="{{url('/cart/checkout')}}">Buy All</a><b>
     </b></div>
  </div>
  <div class="row">
    @foreach($product as $p)
    <div class="col-md-auto mx-auto my-3">
      <div class="card " style="width:200px">
        @if(isset($p->Cart->Photos[0]->url))
        <img class="card-img-top" src="{{ asset('/images/'.$p->Cart->Photos[0]->url) }}" alt="Card image" style="width:100%;height: 198px">
        @else
        <img class="card-img-top" src="{{ asset('/images/specialoffer.png') }}" alt="Card image" style="width:100%">
        @endif
        <img data-id="{{$p->Cart->id}}" class="position-absolute cartdel" src="{{ asset('/images/itemdel.png') }}" width="25px" height="25px" style="left: 93%;bottom: 97%;">
        <div class="card-body">
          <h4 class="card-title">{{$p->Cart->name}}</h4>
          <p class="card-text">Price: {{$p->Cart->price}}$</p>
          <p class="card-text btn-link cartplus">Count: {{$p->count}}</p>
          <p class="card-text btn-link prodtot"><b>Total: {{$p->Cart->price  * $p->count}} $</b></p>
          <div class="btn-group pl-2 ">
            <button  class="icon-btn add-btn add_cartcls" data-id="{{$p->Cart->id}}"  >
            <a class="btn add-icon"></a>
            <a class="btn btn-txt">Add</a>
            </button>
            <button class="icon-btn add-btn remcart" data-id="{{$p->Cart->id}}">
            <a class="btn btn-txt " >Remove</a>
            </button>
          </div>
        </div>
        <div class="btn btn-light movetowishbtn" data-id="{{$p->Cart->id}}" > Move To Wishlist</div>
        <a href="product/item/{{$p->Cart->id}}" class="btn btn-light">Details</a>
      </div>
    </div>
    @endforeach
  </div>
</div>
<div class="row">
  <div class="mx-auto">{{$product->links()}}</div>
</div>
<input type="hidden" id="movetowish" value="{{csrf_token()}}">
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection