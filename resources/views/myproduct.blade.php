@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="container my-5">
  <div class="row">
    @foreach($product as $p)
    <div class="col-md-auto mx-auto my-3">
      <div class="card " style="width:200px">
        @if(isset($p->Photos[0]->url))
        <img class="card-img-top" src="{{ asset('/images/'.$p->Photos[0]->url) }}" alt="Card image" style="width:100%;min-height: 198px">
        @else
        <img class="card-img-top" src="{{ asset('/images/specialoffer.png') }}" alt="Card image" style="width:100%">
        @endif
        <div class="card-body">
          <h4 class="card-title">{{$p['name']}}</h4>
          <p class="card-text">Price: {{$p['price']}}$</p>
          <div class="btn-group">
            <div   data-id="{{$p['id']}}" class="btn btn-outline-secondary add_cartcls" >Add Cart</div>
            <div class="btn btn-outline-secondary add_wishcls" data-id="{{$p['id']}}"  >Add Wishlist</div>
          </div>
        </div>
        <a href="product/item/{id}" class="btn btn-light">Details</a>
      </div>
    </div>
    @endforeach
  </div>
</div>
<div class="row">
  <div class="mx-auto">{{$product->links()}}</div>
</div>
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection