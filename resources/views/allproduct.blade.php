@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="container my-5">
  <div class="row">
    <div class="col"></div>
    <button type="button" class="btn btn-info float-right srchopbut mb-5"><span class="spinner-grow spinner-grow-sm"></span>Search</button>
  </div>
  <div class="row srchbody" style="display: none;">
    <div class="col-sm-6"><img src="{{ asset('/images/srch.png') }}" width="400" height="400"></div>
    <div class="col-sm-6 float-right">
      <div class="row">
        <div class="col">
          <label class="my-4" for="prod_name">Product Name:</label></div>
          <div class="col">
            <input id="prod_name" type="text" class="form-control my-4" name="">
          </div>
        </div>
        <div class="row">
          <div class="col">
            <label class="my-4" for="prod_min_price">Min Price: <b><span style="color: red" id="prod_min_price_show"></span>$</b></label></div>
            <div class="col">
              <input id="prod_min_price" type="range" min="1" max="999" class="form-control my-4" name="">
            </div>
          </div>
          <div class="row">
            <div class="col">
              <label class="my-4" for="prod_max_price">Max Price: <b><span style="color: red" id="prod_max_price_show"></span>$</b></label></div>
              <div class="col">
                <input id="prod_max_price" type="range" min="1" max="999" class="form-control my-4" name="">
              </div>
            </div>
            <button  class="btn btn-outline-info btn-block mt-5" disabled id="prod_srch"> Search</button>
            <input type="hidden" id="token2" value="{{csrf_token()}}">
          </div>
        </div>
        <div class="row allsrch" >
          @foreach($product  as $p)
          <div class="col-md-auto mx-auto my-3">
            <div class="card " style="width:200px">
              @if(isset($p->Photos[0]->url))
              <img class="card-img-top" src="{{ asset('/images/'.$p->Photos[0]->url) }}" alt="Card image" style="width:100%;min-height: 198px">
              @else
              <img class="card-img-top" src="{{ asset('/images/specialoffer.png') }}" alt="Card image" style="width:100%">
              @endif
              <div class="card-body">
                <h4 class="card-title">{{$p['name']}}</h4>
                <p class="card-text">Price: {{$p['price']}}$</p>
                <div class="btn-group">
                  <div   data-id="{{$p['id']}}" class="btn btn-outline-secondary add_cartcls" >Add Cart</div>
                  <div class="btn btn-outline-secondary add_wishcls" data-id="{{$p['id']}}">Add Wishlist</div>
                </div>
              </div>
              <a href="all_product/item/{{$p['id']}}" class="btn btn-light">Details</a>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      <div class="row">
        <div class="mx-auto">{{$product ?? ''->links()}}</div>
      </div>
      @endsection
      <!-- user name surname -->
      @section('name_surname')
      {{$data['name']}}
      {{$data['surname']}}
      @endsection