@extends('layouts.app')
@section('content')
<div class="container">
	<div class="text-center mx-auto" style="width: 50%;height: 100vh" >
		<form  method="post" action="{{url('login')}}" class="form-signin">
			<h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
			<img src="{{ asset('/images/log.png') }}" width="200" height="200" class="my-5">
			<label for="email" class="sr-only">Email Address</label><br>
			<input type="text" name="email" id="email" value="{{old('email')}}" class="form-control" placeholder="Email address" required="" autofocus="">
			<div class="error" style='color:red'>{{$errors->first("email")}}</div>
			<label for="password" class="sr-only">Password:</label><br>
			<input type="password" name="password" id="password" value="{{old('password')}}" class="form-control" placeholder="Password" required="">
			<div class="error" style='color:red'>{{$errors->first("password")}}</div>
			<a href="{{url('password/recover')}}" class="float-right mt-3">Forgot Password</a><br><br><br>
			<a href="{{url('signup')}}" class="float-right">Sign Up</a>
			<button class="btn btn-lg btn-primary btn-block my-5">Sign In</button>
			{{csrf_field()}}
		</form>
	</div>
</div>
@endsection