@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="container my-5">
	@foreach($orderinfo as $o)
	<div class="product-card">
		<div class="image-card">
			@if(isset($o->product->Photos[0]->url))
			<img  src="{{ asset('/images/'.$o->product->Photos[0]->url) }}" width="200px" height="200px">
			@else
			<img  src="{{ asset('/images/specialoffer.png') }}" width="200px" height="200px" >
			@endif
		</div>
		<div class="product-content">
		<header class="product-title">{{$o->product->name}}</header>
		<div class="product-rating">
			<div class='star'></div>
			<div class='star'></div>
			<div class='star'></div>
			<div class='star'></div>
		</div>
		<section class='product-info'>
			<span class='desc'>Lorem ipsum deluxe type with so and so </span>
		</section>
		<section class='product-info'>
			<span class='desc'> {{$o['count']}} / {{$o['price']}} $ </span>
		</section>
		<section class='product-info'>
			<span class='desc'> Order # {{$o['order_id']}} </span>
		</section>
		<section class='product-info'>
			<a href="#"  class="testbtt btn btn-secondary" style="width: 100px;">FeedBack</a>
		</section>
		<div class='seperator'></div>
	</div>
	<div class='test ' style="display:none;box-sizing: border-box;overflow-y: auto;overflow-x: hidden;" >
		<h4>Feedback</h4>
		@if(isset($o['feedback']))
		<div style="max-width: 200px;height: auto;box-sizing: border-box;">{{$o['feedback']}}</div>
		@else
		<div class="form-group">
			<textarea class="form-control" id="orderfeedback" rows="4"></textarea>
		</div>
		<button id="ordid" value="{{$o['id']}}" class="btn btn-primary btn-block feedbutt">Send</button>
		@endif
</div>
</div>
@endforeach
<input type="hidden" id="feedb" value="{{csrf_token()}}">
</div>
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection
<!-- css -->
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/myorder.css') }}">
@endsection
<!-- js -->
@section('js')
<script type="text/javascript" src="{{asset('js/myorder.js')}}"></script>
@endsection