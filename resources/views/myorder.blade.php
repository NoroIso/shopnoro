@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="container my-5">
  <div class="row">
    <div class="col"><b>Order #</b></div>
    <div class="col"><b>Price</b></div>
    <div class="col"><b>Time</b></div>
  </div>
  @foreach($myorder as $o)
  <a href="myorder/item/{{$o['id']}}" class="row my-4 olist">
    <span  class="col olistitem">{{$o['id']}}</span>
    <span  class="col olistitem">{{$o['sum']}}$</span>
    <span  class="col olistitem">{{$o['time']}}</span>
  </a>
  @endforeach
</div>
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection
