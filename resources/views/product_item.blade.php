@extends('layouts.app2')
<!-- body content -->
@section('content')
<div class="container my-5">
  <div class="row">
    <div class="col-sm-4">
      @if(isset($productinfo->Photos[0]->url))
      <img class="card-img-top" src="{{ asset('/images/'.$productinfo->Photos[0]->url) }}" alt="Card image" style="width:100%">
      @else
      <img class="card-img-top" src="{{ asset('/images/specialoffer.png') }}" alt="Card image" style="width:100%">
      @endif
      <button id="openprodphoto" type="button" class="btn btn-secondary btn-block fa fa-arrow-down" aria-hidden="true">All Photos</button>
    </div>
    <div class="col-sm-8">
      <h3>Name  : {{$productinfo['name']}}</h3>
      <h3>Price : {{$productinfo['price']}}$</h3>
      <h3>Stock : {{$productinfo['count']}}</h6>
      <h6>Description : {{$productinfo['description']}}</h3>
      <input type="hidden" id="pr_id" value="{{$productinfo['id']}}">
      <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
        <div class="btn-group">
          <div   data-id="{{$productinfo['id']}}" class="btn btn-outline-secondary add_cartcls" >Add Cart</div>
          <div  class="btn btn-outline-secondary add_wishcls" data-id="{{$productinfo['id']}}"  >Add Wishlist</div>
        </div>
        <div class="btn-group" role="group">
          <button id="btnGroupDrop1" type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Settings
          </button>
          <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <!-- <a class="dropdown-item" href="/update_item/{{$productinfo['id']}}">Edit</a> -->
            <!-- Button to Open the Modal -->
            <a type="button" class="dropdown-item" data-toggle="modal" data-target="#myModal">
              Edit
            </a>
            <a class="dropdown-item" href="">Add Photo</a>
            <a class="dropdown-item" href="/delete_item/{{$productinfo['id']}}">Delete Item</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--  -->
  <div id="divprodphoto" class="row my-5" style="display: none;">
    <div class="col-md-auto mx-auto my-3">
      @if(isset($productinfo->Photos[0]->url))
      @foreach ($productinfo->Photos as $p)
      <div class="mx-3 my-5" style="display:inline-block;position:relative;">
        <img  src="{{ asset('/images/'.$p['url']) }}" alt="Card image" width="200px" height="200px">
        <img data-id="{{$p['id']}}" class="position-absolute prodphotodelete" src="{{ asset('/images/itemdel.png') }}" width="25px" height="25px" style="left: 93%;bottom: 92%;" >
      </div>
      @endforeach
      @else
      <img class="" src="{{ asset('/images/specialoffer.png') }}" alt="Card image" width="200px" height="200px">
      @endif
    </div>
  </div>
  <!--  -->
</div>
<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modal Heading</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="mb-3">
          <label for="name">Name:</label><br>
          <input type="text" name="name" id="name" value="{{$productinfo['name']}}" class="form-control">
        </div>
        <div class="mb-3">
          <label for="price">Price:</label><br>
          <input type="text" name="price" id="price" value="{{$productinfo['price']}}" class="form-control">
        </div>
        <div class="mb-3">
          <label for="count">Count:</label><br>
          <input type="text" name="count" id="count" value="{{$productinfo['count']}}" class="form-control">
        </div>
        <div class="mb-3">
          <label for="description">Description:</label><br>
          <textarea type="text" name="description" id="description"  class="form-control">{{$productinfo['description']}}</textarea>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary prodedt" href="/update_item/{{$productinfo['id']}}">Edit</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="token1" value="{{csrf_token()}}">
<input type="hidden" id="token3" value="{{csrf_token()}}">
@endsection
<!-- user name surname -->
@section('name_surname')
{{$data['name']}}
{{$data['surname']}}
@endsection