<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishlistModel extends Model
{
    public $table="wishlist";
    public $timestamps=false;
    public function Wish(){
    	return $this->hasOne('App\ProductModel','id','product_id');
    }
}
