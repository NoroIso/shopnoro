<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class ProductModel extends Model
{
    public $table="product";
    public $timestamps=false;
    public function Photos(){
    	// mek@ shatin
    	return $this->hasMany('App\ProductPhotoModel','product_id','id');
    }
    public function Seller(){
    	// shat@ mekin
    	return $this->belongsTo('App\UserModel','user_id','id');
    }
}
