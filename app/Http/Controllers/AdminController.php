<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
use App\ProductModel;
class AdminController extends Controller
{
	function admin_view(){
          return view('admin');
    }
    function getUsers(){
          return UserModel::where('type','user')->get();
    }
    function send(Request $r){
          return $r->namak;
    }
    function getProducts(){
          return ProductModel::where('status',0)->get();
    }
    function updateProd($x){
          ProductModel::where('id',$x)->update(['status'=>1]);
    }  
}
