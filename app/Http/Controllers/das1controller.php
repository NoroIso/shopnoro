<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class das1controller extends Controller
{
    function das1_show(){
    	$user = "Selene";
    	$people = ["Sauron", "Rhaynis", "Deynerys"];
    	$users = [
    		["name" => "Visenya", "surname" => "Targaryen", "age" => "30"],
            ["name" => "Robb", "surname" => "Stark", "age" => "21"],
            ["name" => "Harlan", "surname" => "Tyrell", "age" => "30"],
        ];
    	return view ("das1")->with("name",$user)->with("people",$people)->with("users",$users);
    }

    function tpel(){
    	print 1;
    }
}
