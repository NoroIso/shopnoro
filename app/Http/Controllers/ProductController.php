<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\UserModel;
use App\ProductModel;
use App\WishlistModel;
use App\CartModel;
use App\ProductPhotoModel;
use App\OrderModel;
use App\OrderDetailModel;
use Hash;
use Session;
class ProductController extends Controller
{
    function show_add_product(){
        $data = UserModel::where('id', Session::get('id'))->first();
    	return view('add_product')->with('data',$data);
    }
    function add_product(Request $r){
        $data = UserModel::where('id', Session::get('id'))->first();
    	$validation=Validator::make($r->all(),
    		[
    			"name"=>"required",
    			"count"=>"required|integer",
    			"price"=>"required|integer",
    			"description"=>"required|min:20",
    		]);
    		if ($validation->fails()) {
    			return Redirect::to("/add_product")->withErrors($validation)->withInput();
    		}
    		else{
    			$product=new ProductModel();
    			$product->user_id=$data['id'];
    			$product->name=$r->name;
    			$product->count=$r->count;
    			$product->price=$r->price;
    			$product->description=$r->description;
    			$product->save();
                if ($r->hasFile('photo')) {
                    // dd( $r->file('photo'));
                    foreach ($r->file('photo') as $image) {
                        $name=time().$image->getClientOriginalName();
                        $image->move(public_path().'/images',$name);
                        $img=new ProductPhotoModel();
                        $img->url=$name;
                        $img->product_id=$product->id;
                        $img->save();
                    }
                }
                return Redirect::to("/profile");
    		}
    }
    function item($id){
        $data = UserModel::where('id', Session::get('id'))->first();
        $productinfo=ProductModel::where('id',$id )->first();
        return view('product_item')->with('data',$data)->with('productinfo',$productinfo);
    }
    function all_item($id){
        $data = UserModel::where('id', Session::get('id'))->first();
        $productinfo=ProductModel::where('id',$id )->first();
        return view('all_product_item')->with('data',$data)->with('productinfo',$productinfo);
    }
    function delete_item($id){
        ProductModel::where('id',$id )->delete();
        return Redirect::to("/myproduct");
    }
    function update_item(Request $r,$id){
        $validation=Validator::make($r->all(),
            [
                "name"=>"required",
                "count"=>"required|integer",
                "price"=>"required|integer",
                "description"=>"required|min:20",
            ]);
            if ($validation->fails()) {
                print json_encode($validation->errors());
            }
            else{
                 ProductModel::where('id', $id)->update(['name' => $r->name, 'count' => $r->count, 'price' => $r->price, 'description' => $r->description]);
            }
    }
    function addTocart(Request $r){
        $data = UserModel::where('id', Session::get('id'))->first();
        $thiscart = CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id)->first();
        $productinfo=ProductModel::where('id',$r->prod_id )->first();

        
            if (isset($thiscart)) {
                if ($productinfo->count-1<$thiscart->count) {

                }else{
                    CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id)->increment('count');
                    $count = CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id)->first();
                     $produ=ProductModel::where('id',$r->prod_id )->first();
                        $arr['count']=$count->count;
                        $arr['price']=$produ->price;
                        return $arr;
                    
                }
        }else{
            $cart=new CartModel();
            $cart->user_id=$data['id'];
            $cart->product_id=$r->prod_id;
            $cart->count=1;
            $cart->save();
            return 1;
        }
    }
    function addTowishlist(Request $r){
        $data = UserModel::where('id', Session::get('id'))->first();
        $thiswish = WishlistModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id)->first();
        if (isset($thiswish)) {
        return $thiswish;
        }else{
            $wish=new WishlistModel();
            $wish->user_id=$data['id'];
            $wish->product_id=$r->prod_id;
            $wish->save();
        }
    }
    function prod_srch(Request $r){
       $validation=Validator::make($r->all(),
            [
                "prod_name"=>"required",
            ]);
       if ($validation->fails()) {
                print json_encode($validation->errors());
            }
            else{
                $data = ProductModel::where('name', 'LIKE', '%' . $r->prod_name . '%')->whereBetween('price', [$r->prod_min_price, $r->prod_max_price])->get();
                $arr=[];
                foreach ($data as $k ) {
                    $k->Photos;
                    $arr[]=$k;
                }
                return $arr;
            }
    }
    function pho_del_product(Request $r){
       ProductPhotoModel::where('id',$r->prodpho)->delete();
    }
    function move_to_cart(Request $r){
        $data = UserModel::where('id', Session::get('id'))->first();
        $thiscart = CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id)->first();
        $productinfo=ProductModel::where('id',$r->prod_id )->first();

        if (isset($thiscart)) {
            if ($productinfo->count-1<$thiscart->count) {
                return 1;
                }else{
                WishlistModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id )->delete();   
                CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id)->increment('count');
                }
        }else{
            WishlistModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id )->delete();   
            $cart=new CartModel();
            $cart->user_id=$data['id'];
            $cart->product_id=$r->prod_id;
            $cart->count=1;
            $cart->save();
        }
    }
    function move_to_wish(Request $r){
        CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id )->delete();
        $data = UserModel::where('id', Session::get('id'))->first();
        $thiswish = WishlistModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id)->first();
        if (isset($thiswish)) {
        return $thiswish;
        }else{
            $wish=new WishlistModel();
            $wish->user_id=$data['id'];
            $wish->product_id=$r->prod_id;
            $wish->save();
        }
    }
    function del_cart(Request $r){
        CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id )->delete();
    }
    function rem_cart(Request $r){
        CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id )->decrement('count');
        $count = CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id)->first();
        
        if ($count->count==0) {
        CartModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id )->delete();
        }else{
            $productinfo=ProductModel::where('id',$r->prod_id )->first();
            $arr['count']=$count->count;
            $arr['price']=$productinfo->price;
            return $arr;
        }
    }
    function del_wish(Request $r){
        WishlistModel::where('user_id', Session::get('id'))->where('product_id',$r->prod_id )->delete();
    }
    function order_item($id){
        $data = UserModel::where('id', Session::get('id'))->first();
        $orderinfo = OrderDetailModel::where('order_id',$id )->get();
        return view('order_item')->with('data',$data)->with('orderinfo',$orderinfo);
    }
    function feedback(Request $r){
        OrderDetailModel::where('id', $r->ordid)->update(['feedback' => $r->orderfeedback]);
        $data = OrderDetailModel::where('id',$r->ordid )->first();
        return $data;
    }
}