<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    function signup_show(){
    	return view("signup");
    }

    function login_show(){
      return view("login");
    }

    function edit_show(){
      $data = shopnoro::where('id', Session::get('id'))->first();
      return view("edit")->with('data',$data);
    }

    function registr(Request $r){
    	//print $r->surname;
    	//dd($r);
    	//dd($r->all());

    	$v = Validator::make($r->all(),
         [
          "name" => "required",
          "surname" => "required",
          "age" => "required|integer",
          "email" => "required|email|unique:users,email",
          "gender" => "required",
          "password" => "required|min:6|max:12",
          "confirm_password" => "required|same:password",
         ]);
    	if ($v->fails()) {
    		return Redirect::to("/signup")->withErrors($v)->withInput();
    	}
    	else{
           $user = new shopnoro();
           $user->name = $r->name;
           $user->surname = $r->surname;
           $user->age = $r->age;
           $user->email = $r->email;
           $user->gender = $r->gender;
           $user->password = Hash::make($r->password);
           $user->save();


           
    	}
    }
    function getlogin(Request $l){
      $v = Validator::make($l->all(),
       ["email" => "required", "password" => "required|min:6|max:12"]);
      $data = shopnoro::where('email',$l->email)->first();
      $v->after(function($v) use($data,$l){
        if(!$data){
          $v->errors()->add('email','chka tebnc mail');
        }
        else if(!Hash::check($l->password,$data['password'])){
          $v->errors()->add('password','sxal parol');

        }

      });
      if ($v->fails()) {
        return Redirect::to("/login")->withErrors($v)->withInput();
      }
      else{
        Session::put('id',$data['id']);
        return Redirect::to("/profile");
      }
    }


    function profile_show(){
      $data = shopnoro::where('id',Session::get('id'))->first();
      return view("profile")->with('data',$data);

    }

    function logout(){
      session()->flush();
      return Redirect::to("/login");
    }

    function edit(Request $s){
       $u_id=Session::get('id');
       $valid = Validator::make($s->all(),
        [
          "name" => "required",
          "surname" => "required",
          "age" => "required|integer",
          "email" => "required|email",

        ]);
       $mail = shopnoro::where('id',Session::get('id'))->first();
       $data = shopnoro::where('email',$s->email)->where('email',"!=", $mail->email)->first();
       $valid->after(function($validation) use($data,$s){
        if ($data) {
          $valid->errors()->add('email',"bazayum arka e nman mail");
        }
       });
       if($valid->fails()){
        return Redirect::to("/edit")->withErrors($valid)->withInput();
       }
       else{
          shopnoro::where('id',$u_id)->update(['name' => $s->name, 'surname' => $s->surname, 'age' => $s->age, 'email' => $s->email]);
          return Redirect::to("/edit");
       }
    }
}
