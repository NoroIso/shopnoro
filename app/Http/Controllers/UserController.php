<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\UserModel;
use App\ProductModel;
use App\WishlistModel;
use App\CartModel;
use App\ProductPhotoModel;
use App\OrderModel;
use App\OrderDetailModel;
use Hash;
use Session;
use Mail;
use Illuminate\Support\Str;
class userController extends Controller
{
    function show_signup(){
    	return view('signup');
    }
    function show_login(){
        return view('login');
    }
    function show_profile(){
        $data = UserModel::where('id', Session::get('id'))->first();
        $product = ProductModel::all();
        return view('profile')->with('data',$data)->with('product',$product);
    }
    function show_edit(){
        $data = UserModel::where('id', Session::get('id'))->first();
        return view('edit')->with('data',$data);
    }
    function show_myproduct(){
        $data = UserModel::where('id', Session::get('id'))->first();
        $product= ProductModel::where('user_id', Session::get('id'))->paginate(20);
        return view('myproduct')->with('data',$data)->with('product',$product);
    }
    function show_allproduct(){
        $data = UserModel::where('id', Session::get('id'))->first();
        $product= ProductModel::paginate(20);
        return view('allproduct')->with('data',$data)->with('product',$product);
    }
    function show_cartpage(){
        $data = UserModel::where('id', Session::get('id'))->first();
        $product= CartModel::where('cart.user_id', Session::get('id'))->paginate(20);
        return view('mycart')->with('data',$data)->with('product',$product);
    }
    function show_wishpage(){
        $data = UserModel::where('id', Session::get('id'))->first();
        $product= WishlistModel::where('wishlist.user_id', Session::get('id'))->paginate(20);
        return view('mywishlist')->with('data',$data)->with('product',$product);
    }
    function show_password_recover(){
         return view('passrecover');
    }
    function show_myorder(){
        $data = UserModel::where('id', Session::get('id'))->first();
        $myorder= OrderModel::where('user_id', Session::get('id'))->get();
        return view('myorder')->with('data',$data)->with('myorder',$myorder);
    }
    function register(Request $r){
    	// dd($r); //cod@ kangnecnum e ev tpum e amen inch
    	//dd($r->all()); // tpum e miayn postov ekac tvyalner@
    	$validation=Validator::make($r->all(),
    		[
    			"name"=>"required",
    			"surname"=>"required",
    			"age"=>"required|integer",
    			"gender"=>"required",
    			"email"=>"required|email|unique:users,email",
    			"password"=>"required|min:6",
    			"config_password"=>"required|same:password",
    		]);
    		if ($validation->fails()) {
    			return Redirect::to("/signup")->withErrors($validation)->withInput();
    		}
    		else{
    			$user=new UserModel();
    			$user->name=$r->name;
    			$user->surname=$r->surname;
    			$user->age=$r->age;
    			$user->gender=$r->gender;
    			$user->email=$r->email;
    			$user->password=Hash::make($r->password);
    			$user->save();
                $info=['name'=>$user->name,'id'=>$user->id,'hash'=>md5($user->id.$user->email)];
                Mail::send('mail',$info,function($m) use($user){
                    $m->to($user->email,"$user->name $user->surname")->subject('Activate Your Account');
                    $m->from('6563shop6563@gmail.com','Shop Admin');

                });
                return Redirect::to("/login");
    		}
    }
    function login(Request $r){
        $validation=Validator::make($r->all(),
            [
            "email"=>"required|email",
            "password"=>"required|min:6",             
            ]);
        $data = UserModel::where('email',$r->email)->first();
        $validation->after(function($validation) use($data,$r){
            if(!$data){
                $validation->errors()->add('email','chka tenc mail');
            }else if(!Hash::check($r->password,$data['password'])){
                $validation->errors()->add('password','sxal parol');
            }
        });
            if ($validation->fails()) {
                return Redirect::to("/login")->withErrors($validation)->withInput();
            }
            else{ 
                if ($data->active=='0') {
                return Redirect::to("/login");
                }
                if ($data->type=='admin') {
                Session::put('id',$data['id']);
                return Redirect::to("/admin");
                    
                }
                if($data->block == 1){
                    if(time()<$data->block_time){
                        return Redirect::to("/login");
                    }
                    else{
                        Session::put('id',$data['id']);
                        UserModel::where('id',$data['id'])->update(['block'=>0]);
                        return Redirect::to("/profile");
                    }
                }
                Session::put('id',$data['id']);
                return Redirect::to("/profile");
                }
            }
    function log_out(){
        session()->flush();
        return Redirect::to("/login");
            }
    function edit(Request $r){
        $user_id=Session::get('id');
        $validation=Validator::make($r->all(),
            [
                "name"=>"required",
                "surname"=>"required",
                "age"=>"required|integer",
                "email"=>"required|email",
            ]);
        $my_mail = UserModel::where('id',Session::get('id'))->first();
        $data = UserModel::where('email',$r->email)->where('email',"!=", $my_mail->email)->first();
        $validation->after(function($validation) use($data,$r){
            if($data){
                $validation->errors()->add('email','bazayum araka e nman mail');
            }
        });
            if ($validation->fails()) {
                return Redirect::to("/edit")->withErrors($validation)->withInput();
            }
            else{
                UserModel::where('id', $user_id)->update(['name' => $r->name, 'surname' => $r->surname, 'age' => $r->age, 'email' => $r->email]);
                return Redirect::to("/edit");
            }
    }
    function changepass(Request $r){
        $user_id=Session::get('id');
        $validation=Validator::make($r->all(),
            [       
                 "oldpassword"=>"required|min:6",
                 "password"=>"required|min:6",
                 "config_password"=>"required|same:password",
            ]);
        $data = UserModel::where('id',Session::get('id'))->first();
        $validation->after(function($validation) use($data,$r){
            if(!Hash::check($r->oldpassword,$data['password'])){
                $validation->errors()->add('oldpassword','sxal parol');
            }
         });
            if ($validation->fails()) {
                print json_encode($validation->errors());
            }
            else{
                UserModel::where('id', $user_id)->update(['password'=>Hash::make($r->password)]);
            }
    }
    function verify_account($hash,$id){
        $user= UserModel::where('id',$id)->first();
        if ($user) {
            if (md5($user->id.$user->email)==$hash) {
                UserModel::where('id',$id)->update(['active'=>1]);
                return Redirect::to("/login");
            }
        }
    }
    function password_recover(Request $r){
        $validation=Validator::make($r->all(),
            [
                "passrecmail"=>"required|email",
            ]);
        $data = UserModel::where('email',$r->passrecmail)->first();
        $validation->after(function($validation) use($data,$r){
            if(!$data){
                $validation->errors()->add('emailmpty','chka tenc mail');
            }
        });
            if ($validation->fails()) {
                print json_encode($validation->errors());
            }
            else{
                $hash=Str::random(32);
                UserModel::where('email', $r->passrecmail)->update(['forgot_token'=>$hash]);
                $info=['id'=>$data->id,'hash'=>$hash];
                Mail::send('passrecmail',$info,function($m) use($r){
                    $m->to($r->passrecmail)->subject('Activate Your Account');
                    $m->from('6563shop6563@gmail.com','Shop Admin');
                });
            }    
    }
function changeforgotenpass(Request $r){
        $data = UserModel::where('email',$r->passrecmail)->first();
        $validation=Validator::make($r->all(),
            [       
                 "email_code"=>"required",
                 "newpss"=>"required|min:6",
                 "newpssconf"=>"required|same:newpss",
            ]);
        $validation->after(function($validation) use($data,$r){
            if($r->email_code!=$data->forgot_token){
                $validation->errors()->add('email_code','Code error');
            }
         });
            if ($validation->fails()) {
                print json_encode($validation->errors());
            }else{
                UserModel::where('email', $r->passrecmail)->update(['password'=>Hash::make($r->newpss)]);
            }
    }




































































































}

