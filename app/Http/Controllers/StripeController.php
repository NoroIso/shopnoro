<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\UserModel;
use App\ProductModel;
use App\WishlistModel;
use App\CartModel;
use App\ProductPhotoModel;
use App\OrderModel;
use App\OrderDetailModel;
use Hash;
use Session;
use Stripe;
class StripeController extends Controller
{
    function stripe_show(){
        $data = UserModel::where('id', Session::get('id'))->first();
        $prod = CartModel::where('user_id',Session::get('id'))->get();
        $sum  = 0;
        foreach ($prod as $key) {
        	$sum+=($key->Cart->price*$key->count);
        }
        Session::put('sum',$sum);
    	return view('stripe')->with('data',$data)->with('sum',$sum);
    }
    function stripePost(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
       $charge =  Stripe\Charge::create ([
                "amount" => 100 * Session::get('sum'),
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from itsolutionstuff.com." 
        ]);
        if ($charge['status'] == 'succeeded') {
        	$order = new OrderModel();
        	$order->user_id = Session::get('id');
        	$order->sum = Session::get('sum');
        	$order->save();
        	$prod = CartModel::where('user_id',Session::get('id'))->get();
        	foreach ($prod as $key) {
        		$od= new OrderDetailModel();
        		$od->order_id=$order->id;
        		$od->product_id=$key->product_id;
        		$od->count=$key->count;
        		$od->price=$key->Cart->price;
        		$od->save();
        		CartModel::where('user_id',Session::get('id'))->delete();
        		ProductModel::where('id',$key->product_id)->update(['count'=>$key->Cart->count-$key->count]);


        	}

      		Session::flash('success', 'Payment successful!');
          
      		return back();
        }

  
    }
}
