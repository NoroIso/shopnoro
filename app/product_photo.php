<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product_photo extends Model
{
    public $table = "product_photo";
    public $timestamps = false;
}
