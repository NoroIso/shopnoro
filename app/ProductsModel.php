 <?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsModel extends Model
{
    public $table = "products";
    public $timestamps = false;
}
