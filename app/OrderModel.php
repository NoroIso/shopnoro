<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    public $table="orders";
    public $timestamps=false;
}
