<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPhotoModel extends Model
{
   	public $table="product_photo";
    public $timestamps=false;
}
